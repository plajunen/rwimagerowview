//
//  RWImageRowView.m
//  ImageRowView
//
//  Created by Petteri Lajunen on 16.12.2015.
//  Copyright © 2015 Outstoke Oy. All rights reserved.
//

#import "RWImageRowView.h"
#import "UIImage+Utility.h"

@interface RWImageRowView ()

@end

@implementation RWImageRowView

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        [self commonInit];
    }
    return self;
}

-(void) awakeFromNib
{
    [self commonInit];
}

-(void) commonInit
{
    self.contentMode = UIViewContentModeRedraw; // Automatically redraw when rect changes
    self.alignment = RWImagesAlignLeft; // By default align to left
    self.imageSize = CGSizeMake(20, 20);
    self.imageMargin = 5.0f;
    self.renderingMode = UIImageRenderingModeAlwaysOriginal;
}

-(void) setImages:(NSArray<UIImage *> *)images
{
    _images = images;
    [self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect
{
    if(self.images.count > 0) {
        CGFloat x = 0;
        CGFloat y = self.frame.size.height / 2.0 - self.imageSize.height / 2.0;
        switch (self.alignment) {
            case RWImagesAlignLeft:
                break;
            case RWImagesAlignFit:
                x = self.imageSize.width / 2.0;
                break;
            case RWImagesAlignCenter:
            {
                CGFloat contentWidth = self.images.count * self.imageSize.width + (self.imageMargin * self.images.count-1);
                x = self.frame.size.width / 2.0 - contentWidth / 2.0;
            }
                break;
            case RWImagesAlignRight:
            {
                CGFloat contentWidth = self.images.count * self.imageSize.width + (self.imageMargin * self.images.count-1);
                x = self.frame.size.width - contentWidth;
            }
                break;
                
            default:
                break;
        }
        
        for(UIImage* image in self.images) {
            
            UIImage* scaledImage = nil;
            if(self.renderingMode == UIImageRenderingModeAlwaysTemplate) {
                scaledImage = [[image tintWithColor:self.tintColor] resizeToSize:self.imageSize];
            } else {
                scaledImage = [image resizeToSize:self.imageSize];
            }
            
            CGRect imageRect = CGRectMake(x, y, scaledImage.size.width, scaledImage.size.height);
            [scaledImage drawInRect:imageRect];
            
            if(self.alignment != RWImagesAlignFit) {
                x += self.imageSize.width + self.imageMargin;
            } else {
                x += self.frame.size.width / self.images.count;
            }
        }
    }
}

@end
