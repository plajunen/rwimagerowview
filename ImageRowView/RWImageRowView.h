//
//  RWImageRowView.h
//  ImageRowView
//
//  Created by Petteri Lajunen on 16.12.2015.
//  Copyright © 2015 Outstoke Oy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    RWImagesAlignLeft,
    RWImagesAlignCenter,
    RWImagesAlignRight,
    RWImagesAlignFit
} RWImagesAlignment;

@interface RWImageRowView : UIView

@property(nonatomic, assign) RWImagesAlignment alignment;
@property(nonatomic, assign) CGSize imageSize;
@property(nonatomic, assign) CGFloat imageMargin;
@property(nonatomic, assign) UIImageRenderingMode renderingMode;
@property(nonatomic, strong) NSArray<UIImage*>* images;

@end
