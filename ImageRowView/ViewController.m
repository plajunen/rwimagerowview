//
//  ViewController.m
//  ImageRowView
//
//  Created by Petteri Lajunen on 16.12.2015.
//  Copyright © 2015 Outstoke Oy. All rights reserved.
//

#import "ViewController.h"
#import "RWImageRowView.h"

@interface ViewController ()
@property(nonatomic, weak) IBOutlet RWImageRowView* imageRow;
@property(nonatomic, weak) IBOutlet RWImageRowView* imageRow2;
@property(nonatomic, weak) IBOutlet RWImageRowView* imageRow3;
@property(nonatomic, weak) IBOutlet RWImageRowView* imageRow4;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.imageRow.tintColor = [UIColor colorWithRed:0.22 green:0.30 blue:0.32 alpha:1.0];
    self.imageRow.renderingMode = UIImageRenderingModeAlwaysTemplate;
    self.imageRow.imageSize = CGSizeMake(18, 18);
    self.imageRow.images = @[[UIImage imageNamed:@"icn_hiking.png"],
                             [UIImage imageNamed:@"icn_kayaking.png"],
                             [UIImage imageNamed:@"icn_mountainbiking.png"],
                             [UIImage imageNamed:@"icn_special_info_dark.png"],
                             [UIImage imageNamed:@"icn_rental_cabin.png"],
                             [UIImage imageNamed:@"icn_shelter.png"],
                             [UIImage imageNamed:@"icn_randonnee.png"],
                             [UIImage imageNamed:@"icn_mountaineering.png"]];

    self.imageRow2.tintColor = [UIColor colorWithRed:0.94 green:0.45 blue:0.0 alpha:1];
    self.imageRow2.alignment = RWImagesAlignCenter;
    self.imageRow2.renderingMode = UIImageRenderingModeAlwaysTemplate;
    self.imageRow2.imageSize = CGSizeMake(18, 18);
    self.imageRow2.imageMargin = 10;
    self.imageRow2.images = @[
                             [UIImage imageNamed:@"icn_shelter.png"],
                             [UIImage imageNamed:@"icn_randonnee.png"],
                             [UIImage imageNamed:@"icn_mountaineering.png"]];

    self.imageRow3.tintColor = [UIColor colorWithRed:0.13 green:0.59 blue:0.80 alpha:1.0];
    self.imageRow3.alignment = RWImagesAlignRight;
    self.imageRow3.renderingMode = UIImageRenderingModeAlwaysTemplate;
    self.imageRow3.imageSize = CGSizeMake(18, 18);
    self.imageRow3.images = @[[UIImage imageNamed:@"icn_hiking.png"],
                             [UIImage imageNamed:@"icn_kayaking.png"],
                             [UIImage imageNamed:@"icn_randonnee.png"],
                             [UIImage imageNamed:@"icn_mountaineering.png"]];
    
    self.imageRow4.tintColor = [UIColor colorWithRed:1 green:0.95 blue:0.09 alpha:1];
    self.imageRow4.alignment = RWImagesAlignFit;
    self.imageRow4.renderingMode = UIImageRenderingModeAlwaysTemplate;
    self.imageRow4.imageSize = CGSizeMake(18, 18);
    self.imageRow4.images = @[[UIImage imageNamed:@"icn_hiking.png"],
                              [UIImage imageNamed:@"icn_kayaking.png"],
                              [UIImage imageNamed:@"icn_mountainbiking.png"],
                              [UIImage imageNamed:@"icn_special_info_dark.png"],
                              [UIImage imageNamed:@"icn_rental_cabin.png"],
                              [UIImage imageNamed:@"icn_randonnee.png"],
                              [UIImage imageNamed:@"icn_mountaineering.png"]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
