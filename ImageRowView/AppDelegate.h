//
//  AppDelegate.h
//  ImageRowView
//
//  Created by Petteri Lajunen on 16.12.2015.
//  Copyright © 2015 Outstoke Oy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

