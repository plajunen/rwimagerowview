//
//  UIImage+Utility.m
//  ImageRowView
//
//  Created by Petteri Lajunen on 16.12.2015.
//  Copyright © 2015 Outstoke Oy. All rights reserved.
//

#import "UIImage+Utility.h"

@implementation UIImage (Utility)

- (UIImage *)tintWithColor:(UIColor *)tintColor {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGRect drawRect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:drawRect];
    [tintColor set];
    UIRectFillUsingBlendMode(drawRect, kCGBlendModeSourceAtop);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

- (UIImage *)resizeToSize:(CGSize)newSize
{
    CGRect newSizeRect = CGRectZero;
    newSizeRect.size = newSize;
    
    CGRect currentSizeRect = CGRectZero;
    currentSizeRect.size = self.size;
    
    CGFloat scale = 0;
    if(self.size.width > self.size.height) {
        scale = self.size.width / newSize.width;
    } else {
        scale = self.size.height / newSize.height;
    }
    
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, self.size.width / scale, self.size.height / scale));
    UIGraphicsBeginImageContextWithOptions(newRect.size, NO, self.scale);
    [self drawInRect:newRect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}


@end
