//
//  UIImage+Utility.h
//  ImageRowView
//
//  Created by Petteri Lajunen on 16.12.2015.
//  Copyright © 2015 Outstoke Oy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utility)
- (UIImage *)tintWithColor:(UIColor *)tintColor;
- (UIImage *)resizeToSize:(CGSize)newSize;

@end
